# Sof3d

Recreating a **3D graphics engine** using:
- [`gloss`](https://hackage.haskell.org/package/gloss), a 2D vector graphics package.

Why:
- To learn how to make 3D graphics.
- Potentially use it to create graphics that would benefit from lazy evaluation.

## Usage

Modify the `src/triangles.sof.mesh` file.

---
**Syntax** is:
- `<vert-name> <x> <y> <z>` (in `#V` block) and 
- `<face-name> <vert-0> <vert-1> <vert-2>` (in `#F` block)

(takes first 4 args, if more than 4 exist)

---
Comments are:
- Anything outside `#V` and `#F` blocks.
- Anything inside `#V` and `#F` blocks that start with `//`. (may be preceded with whitespace)

## Notes
- At the moment the `<z>` is just ignored.
- Will **crash** if:
  - only 1, 2, or 3 args are supplied in any given line in `#V` or `#F`.
  - args `<x>`, `<y>`, or `<z>` in `#V` are not given in a correct `Float` format.
  - arg `<vert-n>` in `#F` doesn't have at least one matching `<vert-name>` in `#V`. (if more than one, will take the first)
- When `#V` or `#F` aren't defined, they'll implicitly be empty.
- When more than one `#V` or `#F` are defined, only the first one is used.

## Licence

```
Copyright (C) 2023 sof

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
```

