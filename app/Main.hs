module Main (main) where

import Data.Char
import Graphics.Gloss

type Vert = (String,Float,Float,Float)
type Face = (String,Vert,Vert,Vert)
type Face' = (String,String,String,String)

scaleFactor :: Float
scaleFactor = 50

main :: IO ()
main = do
        mesh <- readFile "src/triangles.sof.mesh"
        putStrLn . show . snd . parseMeshFile . lines $ mesh
        display (InWindow "Sof3D" (1000, 1000) (100, 100)) black (displayMesh mesh)

displayMesh :: String -> Picture
displayMesh = pictures
            . fmap (color white . displayFace) 
            . snd 
            . parseMeshFile 
            . lines

displayFace :: Face -> Picture
displayFace = polygon . pscale scaleFactor . trimFace

trimFace :: Face -> Path
trimFace (_,(_,ax,ay,_),(_,bx,by,_),(_,cx,cy,_)) = [(ax,ay),(bx,by),(cx,cy)]

parseMeshFile :: [String] -> ([Vert],[Face])
parseMeshFile s = (verts, faces)
                    where 
                        verts = parseVerts vertsblock
                        faces = parseFaces facesblock verts
                        vertsblock = onlyrelevant . takeWhile (/= "#-V") . drop 1 . dropWhile (/= "#V") $ s
                        facesblock = onlyrelevant . takeWhile (/= "#-F") . drop 1 . dropWhile (/= "#F") $ s
                        onlyrelevant = filter (not . prefixes "//" . dropWhile isSpace) . filter (/= "")

parseVerts :: [String] -> [Vert]
parseVerts = fmap parseVert

parseFaces :: [String] -> [Vert] -> [Face]
parseFaces s verts = fmap (\(id,a,b,c) -> (
                            id,
                            getOn (\(v,_,_,_) -> a == v) verts,
                            getOn (\(v,_,_,_) -> b == v) verts,
                            getOn (\(v,_,_,_) -> c == v) verts)
                            ) . fmap parseFace $ s

parseVert :: String -> Vert
parseVert s = (id, read x, read y, read z)
                where (id:x:y:z:_) = words s

parseFace :: String -> Face'
parseFace s = (id, aid, bid, cid)
                where (id:aid:bid:cid:_) = words s


pscale :: Float -> Path -> Path
pscale k = fmap $ \(x,y) -> (k*x,k*y)

getOn :: (a -> Bool) -> [a] -> a
getOn f = head . filter f

prefixes :: Eq a => [a] -> [a] -> Bool
prefixes []     _      = True
prefixes _      []     = False
prefixes (a:as) (b:bs) = if a /= b
                            then False
                            else prefixes as bs
